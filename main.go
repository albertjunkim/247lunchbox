package main

import (
	"bitbucket.org/albertjunkim/247lunchbox/calendar"
	"encoding/json"
	"errors"
	"github.com/hashicorp/consul/api"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"math/rand"

	"github.com/robfig/cron"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Keyboard struct {
	Type    string   `json:"type"`
	Buttons []string `json:"buttons"`
}

type UserPost struct {
	UserKey string `json:"user_key"`
	Type    string `json:"type"`
	Content string `json:"content"`
}

type Message struct {
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

type LunchBoxStatus struct {
	Status  int
	Message string
	Error   string
}

func main() {
	e := echo.New()
	log.New("lunchbox")
	log.EnableColor()
	consul := &Consul{}
	consul.consulSetup()
	c := cron.New()
	c.AddFunc("0 0 7 * * 1", func() {
		status := &LunchBoxStatus{}
		err := Submit(status, consul)
		if err != nil {
			log.Error(err.Error())
		}
	})
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))
	e.GET("/", func(c echo.Context) error {
		bytes, err := consul.get(getYearWeekFormat())
		if err != nil {
			log.Error(err.Error())
			return c.String(http.StatusInternalServerError, err.Error())
		}
		return c.String(http.StatusOK, string(bytes))
	})

	e.GET("/calendar", func(c echo.Context) error {
		msg := make(map[string]map[string]string)
		msg["message"] = make(map[string]string)
		var result string
		events := calendar.GetEvents()
		var prevSundayDate time.Time
		var nextSundayDate time.Time
		if time.Now().Weekday() != time.Sunday {
			hoursToLastSunday := time.Duration(int(time.Now().Weekday())*-24) * time.Hour
			hoursToNextSunday := time.Duration(int(7-time.Now().Weekday())*24) * time.Hour
			prevSundayDate = time.Now().Add(hoursToLastSunday)
			nextSundayDate = time.Now().Add(hoursToNextSunday)
		}

		prev := prevSundayDate.Format("2006-01-02")
		next := nextSundayDate.Format("2006-01-02")

		for _, event := range events {
			if (prev < event.Start) && (event.End <= next) {
				line := event.Start + " " + event.Content
				result += line + "\n"
			}
		}
		msg["message"]["text"] = result
		return c.JSON(http.StatusOK, result)
	})

	e.GET("/test", func(c echo.Context) error {
		status := &LunchBoxStatus{}
		err := Test(status, consul)
		if err != nil {
			return c.String(http.StatusInternalServerError, err.Error())
		}
		return c.String(http.StatusOK, status.Message)
	})

	e.GET("/keyboard", func(c echo.Context) error {
		key := Keyboard{
			Type:    "buttons",
			Buttons: []string{"도시락 제출 현황", "도시락 제출","이번주 누가 빠져?"},
		}
		return c.JSON(http.StatusOK, key)
	})

	e.POST("/message", func(c echo.Context) error {
		u := new(UserPost)
		c.Bind(u)
		msg := make(map[string]map[string]string)
		msg["message"] = make(map[string]string)
		status := &LunchBoxStatus{}
		log.Print(u.Content)
		if strings.Contains(u.Content, "제출") {
			var err error
			status, err = Get(status, consul)
			if err != nil {
				err := Submit(status, consul)
				if err != nil {
					log.Error(err.Error())
					return c.JSON(http.StatusInternalServerError, err.Error())
				} else {
					if status.Status == http.StatusOK {
						msg["message"]["text"] = getYearWeekFormat() + " 우옷옷! 성공 ^ㅇ^ "

					} else {
						msg["message"]["text"] = getYearWeekFormat() + " 도시락 제출 실패 error: " + status.Error
					}
				}
			}else{
				if status.Status == http.StatusOK {
					msg["message"]["text"] = getYearWeekFormat() + " 이미 냄 ^^ "

				} else {
					msg["message"]["text"] = getYearWeekFormat() + " 도시락 제출 실패 error: " + status.Error
				}
			}


		} else if strings.Contains(u.Content, "도시락") {
			var err error
			status, err = Get(status, consul)
			if err != nil {
				msg["message"]["text"] = getYearWeekFormat() + " 아직 안냄 ㅎㅎ;;"
				return c.JSON(http.StatusOK, msg)
			}else {
				if status.Status == http.StatusOK {
					msg["message"]["text"] = getYearWeekFormat() + " 냈음 ㅇㅅㅇ "

				} else {
					msg["message"]["text"] = getYearWeekFormat() + " 도시락 제출 실패 error: " + status.Error
				}
			}
		} else if strings.Contains(u.Content, "빠져") || strings.Contains(u.Content, "누가") || strings.Contains(u.Content, "빠지는") || strings.Contains(u.Content, "무슨일") {
			var result string
			events := calendar.GetEvents()
			var prevSundayDate time.Time
			var nextSundayDate time.Time
			if strings.Contains(u.Content, "다음주") || strings.Contains(u.Content, "담주") {
				if time.Now().Weekday() != time.Sunday {
					hoursToLastSunday := time.Duration(int(time.Now().Weekday())*-24) * time.Hour
					hoursToNextSunday := time.Duration(int(7-time.Now().Weekday())*24) * time.Hour
					nextWeek := time.Duration(7*24) * time.Hour
					prevSundayDate = time.Now().Add(nextWeek).Add(hoursToLastSunday)
					nextSundayDate = time.Now().Add(nextWeek).Add(hoursToNextSunday)
				}
			} else {
				if time.Now().Weekday() != time.Sunday {
					hoursToLastSunday := time.Duration(int(time.Now().Weekday())*-24) * time.Hour
					hoursToNextSunday := time.Duration(int(7-time.Now().Weekday())*24) * time.Hour
					prevSundayDate = time.Now().Add(hoursToLastSunday)
					nextSundayDate = time.Now().Add(hoursToNextSunday)
				}
			}

			prev := prevSundayDate.Format("2006-01-02")
			next := nextSundayDate.Format("2006-01-02")

			for _, event := range events {
				if (prev < event.Start) && (event.End <= next) {
					line := event.Start + " " + event.Content
					result += line + "\n"
				}
			}
			msg["message"]["text"] = result
			return c.JSON(http.StatusOK, msg)
		} else {
			words := []string{"뭐라는 거여", "??? 왓?", "룰루랄라", "호잇호잇", "흠칫뿡!", "^0^", "시작은 미약하나 끝은 창대하리라", "으아아!!", "쿠오오오!!", "삐역삐약", "꾸엑!!", "247247247247"}
			msg["message"]["text"] = words[rand.Intn(13)]
			return c.JSON(http.StatusOK, msg)
		}

		return c.JSON(http.StatusOK, msg)

	})

	e.GET("/submit", func(c echo.Context) error {
		msg := make(map[string]map[string]string)
		msg["message"] = make(map[string]string)
		status := &LunchBoxStatus{}

		err := Submit(status, consul)
		if err != nil {
			log.Error(err.Error())
			return c.JSON(http.StatusInternalServerError, err.Error())
		}

		Status, err := Get(status, consul)
		if err != nil {
			msg["message"]["text"] = getYearWeekFormat() + " 도시락이 아직 제출이 안됬습니다"
			return c.JSON(http.StatusOK, msg)
		}

		if Status.Status == http.StatusOK {
			msg["message"]["text"] = getYearWeekFormat() + " 도시락이 성공적으로 제출 되었습니다 "
		} else {
			msg["message"]["text"] = getYearWeekFormat() + " 도시락 제출 실패 error: " + Status.Error
		}

		return c.JSON(http.StatusOK, msg)
	})

	e.Logger.Fatal(e.Start(":1323"))

}

func submit() (statusCode int, body string, err error) {
	url := "https://docs.google.com/forms/d/e/1FAIpQLSeLGYd5tiRz4Xijsaxik-XJdM8hiD34IQvsNO_QEt_QBzo3-w/formResponse"

	payload := strings.NewReader("entry.517842212=%EA%B0%80%EC%A0%95%20%EB%B0%8F%20%EC%B2%AD%EB%85%84%20-%20%EC%B0%AC%EC%96%91%ED%8C%80%20(%EC%9D%B4%EC%84%B1%EC%97%BD%20%EA%B0%84%EC%82%AC)&entry.1471876270=1%EB%B6%80%20%EC%98%88%EB%B0%B0%20%ED%9B%84&entry.1842074906=PS11&entry.458816439=15&entry.737726878=%EA%B9%80%EC%A4%80&entry.1304361480=8457296121&entry.1823922038=albertjunkim%40gmail.com")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("cookie", "S=spreadsheet_forms%3D7s6rm5swyYAB8wNcsoDLfztmC_7zvNVV; NID=138%3DYhV39z8FRn4QS_4osTGAzzDSV1oi7AyKHxTfaA7XmlhrvLHRJHyMqLPK5txh9T8RMYLGU_cFXQnMw-bX68q9_V2Fyc1umLiARvvhA2Y6NCfXpZHpFfS0XoOYXs5IRicJ")
	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, err := http.DefaultClient.Do(req)

	defer res.Body.Close()
	bodyByte, _ := ioutil.ReadAll(res.Body)

	if err != nil {
		return 0, "", err
	}

	return res.StatusCode, string(bodyByte), nil
}

type Consul struct {
	client *api.Client
	kv     *api.KV
}

func (c *Consul) consulSetup() {
	cfg := api.DefaultConfig()
	c.client, _ = api.NewClient(cfg)
	c.kv = c.client.KV()
}
func (c *Consul) get(key string) ([]byte, error) {
	pair, _, err := c.kv.Get(key, nil)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}
	if pair == nil {
		return nil, errors.New("key does not exist")
	} else {

		return pair.Value, nil
	}

}

func (c *Consul) put(key string, value []byte) error {
	pair := api.KVPair{
		Key:   key,
		Value: value,
	}
	_, err := c.kv.Put(&pair, nil)
	return err
}

func getYearWeekFormat() string {
	year, week := time.Now().ISOWeek()
	format := strconv.Itoa(year) + "-" + strconv.Itoa(week)
	return format
}

func Submit(status *LunchBoxStatus, consul *Consul) error {
	statusCode, _, err := submit()
	if err != nil {
		status.Error = err.Error()
	}
	status.Status = statusCode

	if status.Status == http.StatusOK {
		log.Print("success submitting")
	} else {
		log.Print("error submitting")
	}
	log.Info(getYearWeekFormat())
	jsonByte, _ := json.Marshal(status)
	log.Info(string(jsonByte))
	errs := consul.put(getYearWeekFormat(), jsonByte)
	if errs != nil {
		log.Error(errs.Error())
		return errs
	}
	return nil

}

func Test(status *LunchBoxStatus, consul *Consul) error {
	status.Status = 200
	status.Message = "hello"
	status.Error = ""
	if status.Status == http.StatusOK {
		log.Info("success submitting")
	}
	log.Info(getYearWeekFormat())
	jsonByte, _ := json.Marshal(status)
	log.Info(string(jsonByte))
	errs := consul.put(getYearWeekFormat(), jsonByte)
	if errs != nil {
		log.Error(errs.Error())
		return errs
	}
	return nil

}

func Get(status *LunchBoxStatus, consul *Consul) (*LunchBoxStatus, error) {
	bytes, err := consul.get(getYearWeekFormat())
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}
	json.Unmarshal(bytes, status)
	return status, nil
}
